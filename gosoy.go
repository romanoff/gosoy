package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) == 1 && args[0] == "js" {
		CreateJs()
	} else if len(args) == 3 && args[0] == "soytojs" {
		SoyToJs(args[1], args[2])
	} else if len(args) == 2 {
		RenderTemplate(args[0], args[1])
	} else {
		fmt.Println("Usage: gosoy [js|soytojs <soy_folder> <js_folder>|<template> <json>]")
	}
}
