package main

import "testing"

func TestTemplateName(t *testing.T) {
	template := `
{namespace p.ui.ProductTemplate}

/**
 * Product template
 * @param imageUrl Product image url
 * @param name Product name
 * @param url Product url
 * @param companyName Company name
 * @param companyUrl Company url
 * @param productLinkId Product link id
 * @param companyLinkId Company link id
 */
{template .product}
<div class="p_application">
  <div class="p_application_image_container">
    <img class="p_application_image_container" src="{$imageUrl}" />
  </div>
  <div class="p_application_content">
    <div class="p_application_content_header">
      <a class="p_application_content_header_link" id="{$productLinkId}" href="{$url}">{$name}</a>
    </div>
    <div class="p_application_content_company">
      <a href="{$companyUrl}" class="p_application_content_company_link" id="{$companyLinkId}">{$companyName}</a>
    </div>
    <div class="p_application_content_rating"></div>
  </div>
</div>
{/template}
`
	if templateName(template) != "p.ui.ProductTemplate.product" {
		t.Errorf("Expected to get p.ui.ProductTemplate.product, but got %v", templateName(template))
	}
}
