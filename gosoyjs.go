package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var soyFiles []string

func visitSoy(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, ".soy") {
			soyFiles = append(soyFiles, path)

		}
	}
	return nil
}

func CreateJs() {
	path, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = filepath.Walk(path, visitSoy)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	args := []string{"-jar", "/home/andrey/Programming/closure/templates/build/SoyToJsSrcCompiler.jar", "--outputPathFormat", "templates.js"}
	for _, soyFile := range soyFiles {
		args = append(args, soyFile)
	}
	cmd := exec.Command("java", args...)
	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	soyutilsPath := "/home/andrey/Programming/closure/templates/javascript/soyutils.js"
	soyutils, err := ioutil.ReadFile(soyutilsPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	templatesPath := path + "/templates.js"
	templates, err := ioutil.ReadFile(templatesPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var perm os.FileMode = 0774
	err = ioutil.WriteFile(path+"/templates.js", []byte(string(soyutils)+"\n"+string(templates)+"\n"+NODESERVER), perm)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
