package main

var NODESERVER = `
var http = require('http'), url = require('url');
var qs = require('querystring');
http.createServer(function (req, res) {

    if (req.method == 'POST') {
        var body = '';
        req.on('data', function (data) {
            body += data;
        });
        req.on('end', function () {
            var params = qs.parse(body);
	    var func = params.func;
	    var jsonParams = params.json_params;
	    if (func && jsonParams) {
		try {
		    res.end(eval(func)(eval("("+jsonParams+")")));
		} catch (e) {
		    console.log('Something wrong with params')
		    res.end('');
		}
	    } else {
		res.end('');
	    }
        });
    } else {
	var params = url.parse(req.url, true).query;
	var func = params.func;
	var jsonParams = params.json_params;
	if (func && jsonParams) {
	    try {
		res.end(eval(func)(eval("("+jsonParams+")")));
	    } catch (e) {
		console.log('Something wrong with params')
		res.end('');
	    }
	} else {
	    res.end('');
	}
    }

}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
`
