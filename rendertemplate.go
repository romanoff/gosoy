package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
)

func templateName(content string) string {
	namespaceRe := regexp.MustCompile("{namespace\\s(.*)}")
	templateRe := regexp.MustCompile("{template\\s(.*)}")
	lines := strings.Split(content, "\n")
	namespace := ""
	template := ""
	for _, line := range lines {
		matches := namespaceRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			namespace = strings.TrimSpace(matches[1])
		}
		matches = templateRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			template = strings.TrimSpace(matches[1])
		}
	}
	return namespace + template
}

func RenderTemplate(path string, jsonParams string) {
	templateContent, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	data := url.Values{"func": {templateName(string(templateContent))}, "json_params": {jsonParams}}
	resp, err := http.PostForm("http://localhost:1337/", data)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(content))
}
