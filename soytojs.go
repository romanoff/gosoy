package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type ExistingJs struct {
	Name string
	Path string
}

var existingJsFiles []*ExistingJs

func visitJs(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, ".js") {
			existingJsFiles = append(existingJsFiles, &ExistingJs{filename, path})

		}
	}
	return nil
}

func SoyToJs(soyPath string, jsPath string) {
	err := filepath.Walk(jsPath, visitJs)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = filepath.Walk(soyPath, visitSoy)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	newJs := make([]string, 0, 0)
	for _, soyFile := range soyFiles {
		h := md5.New()
		content, err := ioutil.ReadFile(soyFile)
		io.WriteString(h, string(content))

		jsTemplatePath := fmt.Sprintf("%v/%x.js", jsPath, h.Sum(nil))
		newJs = append(newJs, fmt.Sprintf("%x.js", h.Sum(nil)))
		_, err = os.Stat(jsTemplatePath)
		if err != nil {
			args := []string{"-jar", "/home/andrey/Programming/closure/templates/build/SoyToJsSrcCompiler.jar", "--shouldGenerateJsdoc", "--shouldProvideRequireSoyNamespaces", "--outputPathFormat", jsTemplatePath, soyFile}
			cmd := exec.Command("java", args...)
			err = cmd.Run()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
	}
	for _, oldJs := range existingJsFiles {
		fileFound := false
		for _, jsName := range newJs {
			if jsName == oldJs.Name {
				fileFound = true
			}
		}
		if !fileFound {
			os.Remove(oldJs.Path)
		}
	}
}
